import ase.geometry
import numpy as np
import ase
from ase.neighborlist import natural_cutoffs
from ase.io import read, write
from ase.visualize import view
from ase import neighborlist
from scipy import sparse
from numpy.linalg import norm
import matplotlib.pylab as plt

def shiftCenter(a,new_center):
    '''This function
    shifts the position of
    each atom in an ase.Atoms
    object.
    Input: Atom object and new_center
    Output: Atom object with new positions
    '''
    new_pos=[]
    for x_old,y_old,z_old in a.get_positions():
        x=x_old-new_center[0]
        y=y_old-new_center[1]
        z=z_old-new_center[2]
        new_pos.append((x,y,z))
    return a.set_positions(new_pos)
# Make cell and put all atoms in, centering upon the rx site
def add_cell(a,cell,recenter=[0,0,0]):
    '''This function adds a Cell to 
    each Atoms object (all along the
    simulation) and shifts all atomic
    positions into a newly defined center
    by calling shitCenter(a,new_center).
    Input: a :: ase.Atoms,cell :: list len =3,
    recenter :: list len=3.
    Output: Updated ase.Atoms object.
    '''
    shiftCenter(a,recenter) # center to atom and cell
    a.set_cell(cell)
    a.set_pbc([1,1,1])
    a.wrap()
    return a
# Some handy functions
def inRange(v,rang):
    if min(rang) < v < max(rang): return True
    else: return False
def inBox(lims,A):
    inside = []
    for a in A:
        if  inRange(a.x,lims[0]) and inRange(a.y,lims[1]) and inRange(a.z,lims[2]):
            inside.append(a.index)
    return inside
def inSph(rads,A,c):
    inside = []
    for a in A:
        if inRange(np.sqrt((a.x-c[0])**2+(a.y-c[1])**2+(a.z-c[2])**2),rads):
            inside.append(a.index)
    return inside
def inLay(lims_z,A):
    inside = []
    for at in A:
        if inRange(at.z,lims_z):
            inside.append(at.index)
    return inside

def RebuildMolec(A):
    '''Function that reconstructs the molecules out of the atoms
It is slow, so better to just call it once at the 
beginning of the main func.'''
    cutOff = natural_cutoffs(A)
    neighborList = neighborlist.NeighborList(cutOff, self_interaction=False, bothways=True)
    neighborList.update(A)
    matrix = neighborList.get_connectivity_matrix()
    n_components, component_list = sparse.csgraph.connected_components(matrix)
    return n_components, component_list

def WatInside(A,spheres,cell_p):# A is an Atoms object
    '''
    ============================== WatInside =========================================
    Input:
    A :::::::::::::: ase atoms object
     spheres, 
    cell_p ::::::::: cell parameters
    *_trigger :::::: logic True/False
    Output:
    3 numpy arrays containing atom indeces from A according to the selection patterns
    Summary:
    For each selection pattern, e.g. shell-like boxes, spheres or just layers
    this function will return the indeces of the atoms, oxy-atoms and water molecules
    whose oxy-atoms lie inside the selections.
    E.g.
    Imagine I want to select the 3 layers the next system:
       CELL             SELECTION
    ::::::::::         ::::::::::     
    :        :         :        :
    :        :         :        :
    :oooooooo:   f()   :--------:
    :oooooooo:  ---->  :oooooooo:  
    :oooooooo:         :--------:
    ::::::::::         :oooooooo:
                       :--------:
                       :oooooooo:
                       :--------:
                       ::::::::::
    where f() is this function.
    Same for xyz and radial shells.
    =================================================================================='''
    c = cell_p/2 # center of the cell
    n_components, component_list = RebuildMolec(A)
    #----------------------#
    #:::::: SPHERES :::::::#
    #----------------------#
    # Create some lists
    AT_PERSPH_IN_idx     = [0 for i in spheres]       
    WATERS_PERSPH        = AT_PERSPH_IN_idx[:]
    WATERS_PERSPH_IN_idx = AT_PERSPH_IN_idx[:]
    rads = [0,spheres[0]]
    AT_PERSPH_IN_idx[0] = inSph(rads,A,c)
    for j,sph in enumerate(spheres[1:]):
        j=j+1
        rads = [spheres[j-1],sph]
        AT_PERSPH_IN_idx[j] = inSph(rads,A,c)
    OXY_PERSPH_IN_idxs = [ [idx for idx in idx_a_inside if A[idx].number == 8] for idx_a_inside in AT_PERSPH_IN_idx ]
    WATERS_PERSPH         = [ [component_list[idx] for idx in OXY_idxs] for OXY_idxs in OXY_PERSPH_IN_idxs ]
    WATERS_PERSPH_IN_idx  = [ [i for i in range(len(component_list)) if component_list[i] in waters] for waters in WATERS_PERSPH ]
    # Return data
    return [AT_PERSPH_IN_idx,OXY_PERSPH_IN_idxs,WATERS_PERSPH_IN_idx]

def INST_TEMP(frames,selection,linear=False):
    # frames: list of md steps atoms objects
    # selection: subset of atoms within the atoms objects
    if selection in ['all','All','ALL']:
        selection = [i for i in range(len(frames[0]))] 
    array_T_inst = [0]*len(frames)
    for ia, atoms in enumerate(frames):
        kin_en = atoms[selection].get_kinetic_energy()/0.0005485799096500715*4.3597439393705876e-18
        # units conversion: 1st change mass units to get Har, then move to Joules
        if linear:
            ndof = 3.0*len(atoms[selection])-5.0
        else:
            ndof = 3.0*len(atoms[selection])-6.0
        array_T_inst[ia] = 2*kin_en/ndof/1.3806504e-23 # 2*KE/DoF/kB
        # This result comes from equipartition theorem:
        #  Avg_KE = 1/2 kB*T
        #
    return array_T_inst

def MOVING_AVERAGE(data,window=50):
    '''
    This function will calculate the moving average for 
    a data set within a window, and return de x and y dimensions
    data:   array
    window: bin width to average over (int), the default value is 20
    '''

    if not isinstance(window,int):
        print("Window is not an int. I round it to:",int(window))
        window = int(window)
        
    npoints_average = int(np.floor(len(data)/window))
    if len(data)%window!=0: npoints_average+=1
        
    average_data = []
    xaxis = []
    for ind in range(npoints_average):
        if (ind+1)*window > len(data):
            average_data.append(np.average(data[ind*window:]))
            xaxis.append( ind*window + (len(data)-ind*window)/2 )
        else:
            average_data.append(np.average(data[ind*window:(ind+1)*window]))
            xaxis.append( ind*window + window/2 )

    return xaxis, average_data
def PLOT_ENERGY_AVERAGES(X,Pot,KinIce,KinRg,BE=-1,Other=[-1],units=r"kJ mol$^{-1}$",xlog=False):
    '''
    X: time
    Pot/KinIce/KinRg/BE are energies in kJ/mol
    Other allows the user to plot other energies you must provide a list of [KE,names,color] e.g.,
        other = [[KE_total,r"KE$_TOT$","orange"],[Pot_prod,r"V$_{Prod}$","purple"]]
    '''
    fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(10,6))
    ax.plot(X,[0 for i in X],c='grey',ls="--")
    ax.plot(X,Pot,label=r"$\Delta$V$_{TOT}$",c="green",lw=2)
    ax.plot(X,KinIce,label=r"$\Delta$K$_{Ice}$",c="blue",lw=2)
    ax.plot(X,KinRg,label=r"$\Delta$K$_{Prod}$",c="red",lw=2)
    if BE!=-1:
        ax.plot(X,[BE for i in X],label=r"BE$_{Prod}$",c="k",lw=2)
        ax.legend(ncols=4,loc="lower center")
    else:
        ax.legend(ncol=3,loc="lower center")
    if Other[0]!=-1:
        for case in Other:
            ax.plot(X,case[0],label=case[1],c=case[2],lw=2)
        if BE!=-1:
            ax.legend(ncol=3+len(Other)+1,loc="lower center")
        else:
            ax.legend(ncol=3+len(Other),loc="lower center")


    ax.set_xlim([0,max(X)])
    ax.set_ylabel("Energy ({})".format(units))
    ax.set_xlabel("Time (fs)")
    if xlog:
        ax.set_xscale("log")
        ax.set_xlim([10,max(X)])
    ax.tick_params(axis='both', direction='inout', which='major',bottom=True,top=True,left=True,right=True)
    plt.tight_layout()
    return ax

### Morse/Harmonic quant numb
from scipy.signal import find_peaks
import math

def proper_rounding(n):
    if n-math.floor(n) < 0.5: return math.floor(n)
    else: return math.ceil(n)

def GET_DIST_EVOLUTION(At1,At2,MD,vector=False):
    """
    Returns distances (in Angs) between At1 and At2 over the simulation called MD
    At1 and At2 are indexes (remember that in Py: index = N-1 for N
      the Atom label in e.g. Molden) because lists start at 0, not at 1.
    MD is a list of Atoms objects
    """
    dist = np.zeros(len(MD))
    for i,frame in enumerate(MD):
        dist[i] = frame.get_distance(At1,At2,vector=vector)
    return dist

def MEASURE_FRQ(dist_data,f0,f1,dt=1,which='maxima'):
    """
    dist_data is an array with the distances at each frame
    f0/f1 are the frames of start/end
    dt is the time step, in fs
    which='maxima'/'minima'/'both'
    """
    
    if f1 > len(dist_data): f1=len(dist_data)
    factor = 1.0
    if which=='minima':
        list_indx, rubish = find_peaks(1./np.array(dist_data[f0:f1]))
    elif which=='both':
        list_indx_min, rubish = find_peaks(1./np.array(dist_data[f0:f1]))
        list_indx_max, rubish = find_peaks(np.array(dist_data[f0:f1]))
        # both?
        max_indx = min([len(list_indx_min),len(list_indx_max)])
        list_indx=[]
        if list_indx_min[0]<list_indx_max[0]:
            for i,j in zip(list_indx_min[:max_indx],list_indx_max[:max_indx]):
                list_indx.append(i)
                list_indx.append(j)
                factor = 0.5
        else:
            for i,j in zip(list_indx_max[:max_indx],list_indx_min[:max_indx]):
                list_indx.append(i)
                list_indx.append(j)
                factor = 0.5
    else:
        list_indx, rubish = find_peaks(np.array(dist_data[f0:f1]))
        
    array_frqs = np.array([factor/(list_indx[i]-list_indx[i-1])/dt for i,x in enumerate(list_indx) if i>0])
    return np.average(array_frqs) * 1e15 # Calculate average frq and convert to Hz
def MEASURE_MaxDist(dist_data,f0,f1,dt=1):
    """
    dist_data is an array with the distances at each frame
    f0/f1 are the frames of start/end
    dt is the time step, in fs
    """
    if f1 > len(dist_data): f1=len(dist_data)
    list_indx_maxs, rubish = find_peaks(dist_data[f0:f1])
    return np.average(dist_data[ [i+f0 for i in list_indx_maxs]]) # Calculate average Max displ

def MEASURE_MinDist(dist_data,f0,f1,dt=1):
    """
    dist_data is an array with the distances at each frame
    f0/f1 are the frames of start/end
    dt is the time step, in fs
    """
    if f1 > len(dist_data): f1=len(dist_data)
    list_indx_maxs, rubish = find_peaks(1./np.array(dist_data[f0:f1]))
    return np.average(dist_data[ [i+f0 for i in list_indx_maxs]]) # Calculate average frq and convert to Hz
def MAKE_CHUNKS(fr0,df,simul_length):
    if   float(df*int((simul_length-fr0)/df)) < (simul_length-fr0):
        return [fr0 + i*df for i in range(0,int((simul_length-fr0)/df)+1,1)]+[simul_length]
    elif float(df*int((simul_length-fr0)/df)) > (simul_length-fr0):
        return [fr0 + i*df for i in range(0,int((simul_length-fr0)/df),1)]+[simul_length]
    else:
        return [fr0 + i*df for i in range(0,int((simul_length-fr0)/df)+1,1)]
    
def V_HARMONIC(r,req,f,mu):
    'All units in SI'
    k = (f*2.0*np.pi)**2.0*mu
    return 0.5*k*(r-req)**2.0
def n_HARMONIC(V,f):
    'All units in SI'
    denom = 6.62607015e-34*f
    return proper_rounding(V/denom-0.5)
def V_MORSE(r,req,De,f,mu):
    'All units in SI'
    k = (f*2.0*np.pi)**2.0*mu
    a = np.sqrt( k / 2.0 / De)
    return De * ( 1.0 - np.exp(-a*(r-req)) )**2.0
def n_MORSE_alternative(V,De,f): # unused, non physical solution
    numer = 1.0+np.sqrt(1.0-V/De)
    denom = 6.62607015e-34*f/(2.0*De)
    return proper_rounding(numer/denom-0.5)
def n_MORSE(V,De,f):
    numer = 1.0-np.sqrt(1.0-V/De)
    denom = 6.62607015e-34*f/(2.0*De)
    return proper_rounding(numer/denom-0.5)
    
