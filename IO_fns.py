import ase
import numpy as np

def FIND_LINE(mystr,data=[-1],path=None):
    # Will look for mystr (str or list of str) in 
    #  data or path line per line.
    #print("mystr",mystr)
    if type(mystr) is str:
        leninp = 1
        mystr  = [mystr]
    elif isinstance(mystr,list) or isinstance(mystr,np.ndarray):
        leninp = len(mystr)
    else:
        print("!! Introduce a valid set of mystr (1 or a list)")
    #
    found=[-1]*leninp
    #
    if data==[-1]:
        with open(path,"r") as inp:
            data = inp.readlines()
    elif data==[-1] and path==None:
        print("!! Introduce a valid data list OR file path")
    #
    for ic, case in enumerate(mystr):
        for il, line in enumerate(data):
            if case in line.strip()[0:len(case)] and line.strip()[0]!="#" and found[ic]==-1 and il not in found:
                found[ic] = il
    if leninp==1: return found[0]
    else:         return found

def READ_VALUE_FROM_INP(str_var,data):
    # takes in a string (variable name, as it appears in input), and returns it value
    lnr = FIND_LINE(str_var,data=data)
    # print("lnr",lnr)
    if lnr != -1 and data[lnr].strip()[0]!="#":
        element = data[lnr].strip().split(":")[1]
        if "#" in element:
            element = element.split("#")[0]
        if "," in element:
            element=[i.strip() for i in element.split(",")]
        if type(element) is list: return element
        else: return element.strip()
    else:
        print("Variable {} not found! Default value taken.".format(str_var))
def READ_BOOLEAN_FROM_INP(str_val,data):
    # print(READ_VALUE_FROM_INP(str_val,data=data))
    if READ_VALUE_FROM_INP(str_val,data=data) in ['True','true','TRUE','t','T','1','yes','Yes','YES']:
        return True
    elif READ_VALUE_FROM_INP(str_val,data=data) in ['False','false','FALSE','f','F','0','no','No','NO']:
        return False
    else:
        print("!! Introduce a correct BOOLEAN for {} (True/False)".format(str_val))


def INPUT_READER(path):
    with open(path,'r') as inpfile:
        data = inpfile.readlines()
    keywords = ["positions","velocities",'ener',"output_dir","cell_params","center","1st_frame","Last_frame", "1st_sph", "nr_shells", "time_step", "reagents", "selection"]
    key_bools= ["show_shells"]
    params   = {key:-1 for key in keywords+key_bools}
    for ik, key in enumerate(keywords):
        params[key] = READ_VALUE_FROM_INP(key,data)
        # change formats
        if key=="cell_params": params[key] = [float(i) for i in params[key]]
        if key=="center":
            if isinstance(params[key],list):
                params[key] = [float(i) for i in params[key]]
            else: params[key] = int(params[key])
        if key in ["1st_frame","Last_frame",'nr_shells']: params[key] = int(params[key])
        if key in ["1st_sph","time_step"]: params[key] = float(params[key])
        if key == 'reagents': params[key] = [int(i) for i in params[key]]
        if key == "selection" and isinstance(params[key],list): params[key] = [int(i) for i in params[key]]
    # booleans
    for ik, key in enumerate(key_bools):
        params[key] = READ_BOOLEAN_FROM_INP("show_shells",data)
    return params

##############################################
###### FAST READER FUNCTIONS, MANUALLY CREATED
##############################################
def find_my_frames(inFile):
    location_frame_inFile=[]
    with open(inFile,"r") as data_myfile:
        DATA = data_myfile.readlines()
        nat = int(DATA[0].split()[0])
        for i,line in enumerate(DATA):
            if str(nat) in DATA[i] and len(DATA[i].split())==1:
                location_frame_inFile.append(i)
    return location_frame_inFile, nat
def FAST_POS_VEL_READER(inFile_pos,inFile_vel):
    FR_IND_LOC, NAT = find_my_frames(inFile_pos)
    with open(inFile_pos, "r") as data_pos:    DATA_POS = data_pos.readlines()
    with open(inFile_vel, "r") as data_vel:    DATA_VEL = data_vel.readlines()
    if len(DATA_POS) == len(DATA_VEL):
        ATOMS_LIST = [0 for i in range(len(FR_IND_LOC))]
        for indx, line_nr in enumerate(FR_IND_LOC[:]):
            syms, poss, vels = [0]*NAT, [0]*NAT, [0]*NAT
            for indx2,line in enumerate(range(line_nr+2,line_nr+NAT+2,1)):
                pos = DATA_POS[line].split() # this is a list
                vel = DATA_VEL[line].split() # this is a list
                #
                syms[indx2] = str(pos[0])
                poss[indx2] = (float(pos[1]),float(pos[2]),float(pos[3]))
                vels[indx2] = (float(vel[1]),float(vel[2]),float(vel[3]))
            ATOMS_LIST[indx] = ase.Atoms(symbols=syms, positions=poss, velocities=vels)
        return(ATOMS_LIST)
    else:
        print("ERROR: POS and VEL do not have the same length! ({} vs {})".format(len(DATA_POS),len(DATA_VEL)))
def ENER_READER(ener_file):
    with open(ener_file,'r') as enerFile:
        DATA = enerFile.readlines()
    length = len(DATA)-1
    Time, Kin, Temp, Pot = [0]*length,[0]*length,[0]*length,[0]*length
    for line in DATA[1:]:
        index, time, kin, temp, pot, constq, usedt = line.split()
        Time[int(index)] = float(time)
        Kin[int(index)]  = float(kin)
        Temp[int(index)] = float(temp)
        Pot[int(index)]  = float(pot)
    return Time, Kin, Temp, Pot

###############################
### WRITING FNS
###############################

def SAVE_KE_Shells(list_data,names,outdir,params):
    # list_data and names are lists. prams is a dict
    # The Former contains arrays/lists of kE as a function of time for each sphere/shell
    cell = params["cell_params"]
    SHELL_Rs = params["SHELL_Rs"]
    for n,mat in zip(names,list_data):
        with open(outdir+"/"+n+'.csv','wb') as f:
            head='''Cell size: {}. Units: a.u. 
    Rows:: {} frames ,Cols:: {} sphere like shells. 
    In order to plot you need to transpose the mat.
    Max Radius {}'''.format(cell,len(mat),len(mat[0]),[float('{:.3f}'.format(i)) for i in SHELL_Rs])
            np.savetxt(f, mat.transpose(), fmt='%1.8e',delimiter=',',header=head,comments='#')
def SAVE_KE_Reactants(kinE_react,outdir,params):
    cell = params["cell_params"]
    tstep = params["time_step"]
    with open(outdir+"/"+'reactants_kinE.csv','w') as f:
    #    head='''Cell size: {}. Units: a.u. 
    #Rows:: {} frames ,Cols:: 1.'''.format(cell,len(kinE_react_massindp))
    #    np.savetxt(f,np.asarray([[i*tstep for i in range(len(kinE_react_massindp))],kinE_react_massindp]).transpose(),fmt='%1.8e',delimiter=',',header=head,comments='#')
        head='''Cell size: {}. Units: a.u. 
    Rows:: {} frames ,Cols:: 1.'''.format(cell,len(kinE_react))
        np.savetxt(f,np.asarray([[i*tstep for i in range(len(kinE_react))],kinE_react]).transpose(),fmt='%1.8e',delimiter=',',header=head,comments='#')
def SAVE_KE_Selection(kinE_selH2O,outdir,params):
    cell = params["cell_params"]
    tstep = params["time_step"] 
    with open(outdir+"/"+'selection_kinE.csv','w') as f:
        head='''Cell size: {}. Units: a.u. 
    Rows:: {} frames ,Cols:: 1.'''.format(cell,len(kinE_selH2O))
        np.savetxt(f,np.asarray([[i*tstep for i in range(len(kinE_selH2O))],kinE_selH2O]).transpose(),fmt='%1.8e',delimiter=',',header=head,comments='#')
def SAVE_KE_file(namefile,kinE,outdir,params):
    cell = params["cell_params"]
    tstep = params["time_step"]
    
    if namefile[-3:]!=".csv": namefile+=".csv"
        
    with open(outdir+"/"+namefile,'w') as f:
        head='''Cell size: {}. Units: a.u. 
    Rows:: {} frames ,Cols:: 1.'''.format(cell,len(kinE))
        np.savetxt(f,np.asarray([[i*tstep for i in range(len(kinE))],kinE]).transpose(),fmt='%1.8e',delimiter=',',header=head,comments='#')

def LOAD_CSV_FILES(names):
    # names == filenames
    if not isinstance(names,list): names = [names]
    data = [0]*len(names)
    for n,mat in zip(names,data):
        with open(n,'r') as f:
            mat=np.loadtxt(f,dtype=float,comments='#', delimiter=',').transpose()
            if "#" in f.readline().strip()[0:10]:
                print("Header File {}:".format(n))
                print(f.readline())
    return data
